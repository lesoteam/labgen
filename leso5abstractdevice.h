#ifndef LESO5ABSTRACTDEVICE_H
#define LESO5ABSTRACTDEVICE_H

#include <QString>
#include <QObject>

#include "leso5_types.h"

class LESO5AbstractDevice : public QObject
{
Q_OBJECT

protected:
    bool openFlag;

public:
    LESO5AbstractDevice();
    ~LESO5AbstractDevice();

    virtual bool openDevice(QString deviceName) = 0;
    virtual void closeDevice() = 0;

    virtual bool isOpen() const { return openFlag; }

    virtual void setFrequency(int freq, SIGNAL_CHANNEL_NUM channel, bool setDividerReg = true) = 0;
    virtual void setOffset(int offset, SIGNAL_CHANNEL_NUM channel) = 0;
    virtual void setGain(double amp, SIGNAL_CHANNEL_NUM channel) = 0;
    virtual void setSignalWaveform(SIGNAL_TYPE type, SIGNAL_CHANNEL_NUM channel) = 0;
    virtual void setPhaseOffset(int phase, SIGNAL_CHANNEL_NUM channel) = 0;
    virtual QString DeviceID() = 0;
};

#endif // LESO5ABSTRACTDEVICE_H
