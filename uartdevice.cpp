#include "uartdevice.h"

UARTDevice::UARTDevice()
{
    serialPort.setDataBits(QSerialPort::Data8);
    serialPort.setParity(QSerialPort::NoParity);
    serialPort.setFlowControl(QSerialPort::NoFlowControl);
    serialPort.setStopBits(QSerialPort::OneStop);

    serialPort.setBaudRate(UART_BR);

    deviceID = "";
}

UARTDevice::~UARTDevice()
{
}

void UARTDevice::getDeviceID()
{
    char cmd[CMD_BYTE_NUM] = {0, 0, 0};
    cmd[0] = ADDR_VERSION_REQUEST;
    cmd[1] = 0;
    cmd[2] = 0;
    SEND_CMD(&cmd[0]);
    serialPort.write(&cmd[0], 2);
    serialPort.waitForReadyRead(100);
    deviceID = QString::fromLocal8Bit(serialPort.readAll());
}

bool UARTDevice::openDevice(QString portName)
{
    serialPort.setPortName(portName);
    openFlag = serialPort.open(QIODevice::ReadWrite);
    if(openFlag)
        getDeviceID();

    return openFlag;
}

void UARTDevice::closeDevice()
{
    serialPort.close();
    openFlag = false;
}

void UARTDevice::setFrequency(int freq, SIGNAL_CHANNEL_NUM channel, bool setDividerReg)
{
    int reg = (freq * pow(2, 32))/DAC_FREQ;
    char R0[CMD_BYTE_NUM] = {0, 0, 0};      // Младший Байт
    char R1[CMD_BYTE_NUM] = {0, 0, 0};      // Старший байт

    switch(channel)
    {
    case SIGNAL_A: {
        R0[0] = ADDR_NCOA_FREQ1_R0;
        R1[0] = ADDR_NCOA_FREQ1_R1;
        }
        break;
    case SIGNAL_B: {
        R0[0] = ADDR_NCOB_FREQ1_R0;
        R1[0] = ADDR_NCOB_FREQ1_R1;
        }
        break;
    default: {
        R0[0] = ADDR_NCOA_FREQ1_R0;
        R1[0] = ADDR_NCOA_FREQ1_R1;
        }
    }

    R1[1] = (reg & 0xFF000000)>>24;
    R1[2] = (reg & 0x00FF0000)>>16;

    R0[1] = (reg & 0x0000FF00)>>8;
    R0[2] = (reg & 0x000000FF);

    //write(&R0[0], CMD_BYTE_NUM);
    //write(&R1[0], CMD_BYTE_NUM);

    SEND_CMD(&R0[0]);
    SEND_CMD(&R1[0]);

    /*if(setDividerReg)
    {
        switch(channel)
        {
        case SIGNAL_A:
            R0[0] = ADDR_FREQ_DIVIDER_A;
            break;
        case SIGNAL_B:
            R0[0] = ADDR_FREQ_DIVIDER_B;
            break;
        }

        reg = DAC_FREQ/(2*freq) - 1;
        R0[1] = (reg & 0x0000FF00)>>8;
        R0[2] = (reg & 0x000000FF);

        SEND_CMD(&R0[0]);
    }*/
}

void UARTDevice::setOffset(int offset, SIGNAL_CHANNEL_NUM channel)
{
    char cmd[CMD_BYTE_NUM] = {0, 0, 0};

    switch(channel)
    {
    case SIGNAL_A:
        cmd[0] = ADDR_CHANA_OFFSET;
        break;
    case SIGNAL_B:
        cmd[0] = ADDR_CHANB_OFFSET;
        break;
    default:
        cmd[0] = ADDR_CHANA_OFFSET;
    }

    cmd[1] = (offset & 0x0000FF00)>>8;
    cmd[2] = (offset & 0x000000FF);

     //write(&cmd[0], CMD_BYTE_NUM);
    SEND_CMD(&cmd[0]);
}

void UARTDevice::setPhaseOffset(int phase, SIGNAL_CHANNEL_NUM channel)
{
    /*char cmd[CMD_BYTE_NUM] = {0, 0, 0};

    switch(channel)
    {
    case SIGNAL_A:
        cmd[0] = ADDR_NCOA_PH_OFFSET;
        break;
    case SIGNAL_B:
        cmd[0] = ADDR_NCOB_PH_OFFSET;
        break;
    default:
        cmd[0] = ADDR_NCOA_PH_OFFSET;
    }

    cmd[2] = (phase & 0x0000FF00)>>8;
    cmd[1] = (phase & 0x000000FF);

    SEND_CMD(&cmd[0]);*/
}

void UARTDevice::setGain(double amp, SIGNAL_CHANNEL_NUM channel)
{
    char cmd[CMD_BYTE_NUM] = {0, 0, 0};

    switch (channel)
    {
    case SIGNAL_A:
        cmd[0] = ADDR_CHANA_GAIN;
        break;
    case SIGNAL_B:
        cmd[0] = ADDR_CHANB_GAIN;
        break;
    default:
        cmd[0] = ADDR_CHANA_GAIN;
        break;
    }


    if(amp > 1)
    {
        qDebug() << "AMP > 1 !!!";
        cmd[1] = 0xFF;
        cmd[2] = 0xFF;
    }
    else
    {
        double reg_d = 65535.0f/(1.0f/amp);
        int reg = (double)reg_d;
        cmd[1] = (reg & 0x0000FF00)>>8;
        cmd[2] = (reg & 0x000000FF);
    }

    //write(&cmd[0], CMD_BYTE_NUM);
    SEND_CMD(&cmd[0]);
}

void UARTDevice::setSignalWaveform(SIGNAL_TYPE type, SIGNAL_CHANNEL_NUM channel)
{
    char cmd[CMD_BYTE_NUM] = {ADDR_NCO_FORM, 0, 0};

    switch(channel)
    {
    case SIGNAL_A:
        cmd[2] = (char)type;
        break;
    case SIGNAL_B:
        cmd[2] = ((char)type)<<4;
        break;
    default:
        cmd[2] = (char)type;
    }

    SEND_CMD(&cmd[0]);

    //write(&cmd[0], CMD_BYTE_NUM);
    SEND_CMD(&cmd[0]);

    switch(channel)
    {
    case SIGNAL_A:
        cmd[0] = ADDR_MUX_A;
        break;
    case SIGNAL_B:
        cmd[0] = ADDR_MUX_B;
        break;
    default:
        cmd[0] = ADDR_MUX_A;
    }

    cmd[1] = 1;
    cmd[2] = 1;
    SEND_CMD(&cmd[0]);
}
