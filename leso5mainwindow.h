#ifndef LESO5MAINWINDOW_H
#define LESO5MAINWINDOW_H

#include <QMainWindow>
#include <QMdiArea>

#include "uartdevice.h"
#include "devicesettingstoolbar.h"
#include "lib/qcustomplot.h"
#include "signalplot.h"
#include "leso5channelwidget.h"

namespace Ui {
class LESO5MainWindow;
}

class LESO5MainWindow : public QMainWindow
{
    Q_OBJECT

    UARTDevice device;

    SignalPlot *centralPlot;

    //LESO5ChannelControl *channelA;
    //LESO5ChannelControl *channelB;
    QLabel statusText;
    QLabel deviceID;

    DeviceSettingsToolBar *settingsBox;
    //ChannelToolBar *channelABox;
    //ChannelToolBar *channelBBox;

    QMdiArea *centralWidget;
    LESO5ChannelWidget *channelA;
    LESO5ChannelWidget *channelB;

    SIGNAL_CONFIG channelASettings;
    SIGNAL_CONFIG channelBSettings;

public:
    explicit LESO5MainWindow(QWidget *parent = 0);
    ~LESO5MainWindow();

private:
    Ui::LESO5MainWindow *ui;

    friend class DeviceSettingsToolBar;
};

#endif // LESO5MAINWINDOW_H
