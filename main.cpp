#include "leso5mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LESO5MainWindow w;
    w.show();

    return a.exec();
}
