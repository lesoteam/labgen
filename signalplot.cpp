#include "signalplot.h"

SignalPlot::SignalPlot(SIGNAL_CONFIG *_signal)
{
    signal = _signal;

    setLayout(&hLayout);

    plot = new QCustomPlot;
    signalPlot.plotPtr = plot->addGraph();

    signalPlot.graphPen.setColor(QColor(QRgb(signal->getColor())));
    signalPlot.graphPen.setWidthF(GRAPH_LINE_WIDTH);

    signalPlot.plotPtr->setPen(signalPlot.graphPen);

    signalPlot.axisX = plot->xAxis;
    signalPlot.axisY = plot->yAxis;

    signalPlot.plotPtr->setKeyAxis(signalPlot.axisX);
    signalPlot.plotPtr->setValueAxis(signalPlot.axisY);

    signalPlot.axisX->setLabelColor(QColor(QRgb(signal->getColor())));
    signalPlot.axisX->setTickLabelColor(QColor(QRgb(signal->getColor())));
    signalPlot.axisX->setTickPen(signalPlot.graphPen);
    signalPlot.axisX->setSubTickPen(signalPlot.graphPen);
    signalPlot.axisX->setBasePen(signalPlot.graphPen);
    currentTimeScaleFactor = TIME_SCALE_FACTOR_S;
    signalPlot.axisX->setLabel(tr("Time (S)"));
    signalPlot.axisY->setLabel(tr("Amplitude (V)"));

    signalPlot.axisY->setLabelColor(QColor(QRgb(signal->getColor())));
    signalPlot.axisY->setTickLabelColor(QColor(QRgb(signal->getColor())));
    signalPlot.axisY->setTickPen(signalPlot.graphPen);
    signalPlot.axisY->setSubTickPen(signalPlot.graphPen);
    signalPlot.axisY->setBasePen(signalPlot.graphPen);
    signalPlot.axisX->setRange(X_MIN, X_MAX);
    signalPlot.axisY->setRange(Y_MIN, Y_MAX);
    signalPlot.axisY->setAutoTickStep(false);
    signalPlot.axisY->setTickStep(Y_MAX/5);
    signalPlot.axisX->setAutoTickStep(false);
    signalPlot.axisX->setTickStep(X_MAX/10);

    hLayout.addWidget(plot);

    plot->setBackground(Qt::black);

    frequencyLabel = new QCPItemText(plot);
    frequencyLabel->setBrush(Qt::blue);
    frequencyLabel->setColor(QColor(QRgb(signal->getColor())));
    frequencyLabel->position->setAxes(plot->xAxis, plot->yAxis);
    frequencyLabel->setPositionAlignment(Qt::AlignTop|Qt::AlignHCenter);
    frequencyLabel->setFont(QFont("Times", 14, QFont::Normal));
    frequencyLabel->position->setCoords(plot->xAxis->range().center(), plot->yAxis->range().center());

    upperRedLine = new QCPItemLine(plot);
    upperRedLine->setPen(QPen(Qt::red, RED_LINE_WIDTH));
    upperRedLine->start->setCoords(0, Y_MAX);
    upperRedLine->end->setCoords(plot->yAxis->range().maxRange, Y_MAX);
    plot->addItem(upperRedLine);

    lowerRedLine = new QCPItemLine(plot);
    lowerRedLine->setPen(QPen(Qt::red, RED_LINE_WIDTH));
    lowerRedLine->start->setCoords(0, -Y_MAX);
    lowerRedLine->end->setCoords(plot->yAxis->range().maxRange, -Y_MAX);
    plot->addItem(lowerRedLine);

    updatePlot(signal);
}

SignalPlot::~SignalPlot()
{

}

double SignalPlot::checkReload(double val)
{
    if(val >= Y_MAX)
        val = Y_MAX;
    if(val <= Y_MIN)
        val = Y_MIN;

    return val;
}

void SignalPlot::setVoltageRange(double amp_v)
{
    plot->yAxis->setRange(-amp_v, amp_v);
    plot->yAxis->setTickStep(amp_v/5);
}

void SignalPlot::redefineTimeScale(double period)
{
    if(period < 0.5f)
    {
        if(period < 0.0001)
        {
            currentTimeScaleFactor = TIME_SCALE_FACTOR_uS;
            signalPlot.axisX->setLabel(tr("Time (uS)"));
        }
        else
        {
            currentTimeScaleFactor = TIME_SCALE_FACTOR_mS;
            signalPlot.axisX->setLabel(tr("Time (mS)"));
        }
    }
    else
    {
        currentTimeScaleFactor = TIME_SCALE_FACTOR_S;
        signalPlot.axisX->setLabel(tr("Time (S)"));
    }
}

void SignalPlot::buildSinus(SIGNAL_CONFIG *s)
{
    x.clear();
    y.clear();
    double period = 1.0f/(s->currentFrequency);
    redefineTimeScale(period);

    double step = (period * PERIOD_NUM)*(double)currentTimeScaleFactor/POINT_NUM;
    double gain = s->gain;
    double offset = ((double)s->offset) * MAX_AMP_V/MAX_OFFSET_VALUE;

    for(int i(0); i < POINT_NUM; i++)
    {
        x.append(i*step);
        y.append(checkReload(MAX_AMP*gain*sin(2*PI*i*step/(period*(double)currentTimeScaleFactor)) + offset));
    }

    signalPlot.plotPtr->clearData();
    signalPlot.axisX->setRange(0, period*PERIOD_NUM*(double)currentTimeScaleFactor);
    signalPlot.axisX->setTickStep(period*PERIOD_NUM*(double)currentTimeScaleFactor/10);
    signalPlot.plotPtr->setData(x, y);
}

void SignalPlot::buildSAW(SIGNAL_CONFIG *s)
{
    x.clear();
    y.clear();

    double onePeriod = 1.0f/(s->currentFrequency);
    redefineTimeScale(onePeriod);

    double gain = s->gain;
    double stepX = onePeriod*(double)currentTimeScaleFactor/POINT_NUM;
    double stepY = 2*MAX_AMP*gain/POINT_NUM;
    double offset = ((double)s->offset) * MAX_AMP_V/MAX_OFFSET_VALUE;

    double Y = -MAX_AMP*gain;
    double X = 0;

    for(int i(0); i <= (POINT_NUM+1)*PERIOD_NUM; i++)
    {

        if(Y >= MAX_AMP*gain)
        {
            Y = -MAX_AMP*gain;
        }
        else
        {
            Y += stepY;
        }

        x.append(X);
        y.append(checkReload(Y + offset));

        X += stepX;
    }

    signalPlot.plotPtr->clearData();
    signalPlot.axisX->setRange(0, onePeriod*(double)currentTimeScaleFactor*PERIOD_NUM);
    signalPlot.axisX->setTickStep(onePeriod*(double)currentTimeScaleFactor*PERIOD_NUM/10);
    signalPlot.plotPtr->setData(x, y);
}

void SignalPlot::buildRevSAW(SIGNAL_CONFIG *s)
{
    x.clear();
    y.clear();

    double onePeriod = 1.0f/(s->currentFrequency);
    redefineTimeScale(onePeriod);
    double gain = s->gain;
    double stepX = onePeriod*(double)currentTimeScaleFactor/POINT_NUM;
    double stepY = 2*MAX_AMP*gain/POINT_NUM;
    double offset = ((double)s->offset) * MAX_AMP_V/MAX_OFFSET_VALUE;

    double Y = MAX_AMP*gain;
    double X = 0;

    for(int i(0); i <= (POINT_NUM+1)*PERIOD_NUM; i++)
    {

        if(Y <= -MAX_AMP*gain)
        {
            Y = MAX_AMP*gain;
        }
        else
        {
            Y -= stepY;
        }

        x.append(X);
        y.append(checkReload(Y + offset));

        X += stepX;
    }

    signalPlot.plotPtr->clearData();
    signalPlot.axisX->setRange(0, onePeriod*(double)currentTimeScaleFactor*PERIOD_NUM);
    signalPlot.axisX->setTickStep(onePeriod*(double)currentTimeScaleFactor*PERIOD_NUM/10);
    signalPlot.plotPtr->setData(x, y);
}

void SignalPlot::buildTriangle(SIGNAL_CONFIG *s)
{
    x.clear();
    y.clear();

    double onePeriod = 1.0f/(s->currentFrequency);
    redefineTimeScale(onePeriod);
    double gain = s->gain;
    double stepX = onePeriod*(double)currentTimeScaleFactor/POINT_NUM;
    double stepY = 2*2*MAX_AMP*gain/POINT_NUM;
    double offset = ((double)s->offset) * MAX_AMP_V/MAX_OFFSET_VALUE;

    double Y = -MAX_AMP*gain;
    double X = 0;

    bool forwardFlag = true;

    for(int i(0); i <= (POINT_NUM+1)*PERIOD_NUM; i++)
    {

        if(Y >= MAX_AMP*gain)
        {
            forwardFlag = false;
        }

        if(Y <= -MAX_AMP*gain)
        {
            forwardFlag = true;
        }

        if(forwardFlag)
            Y += stepY;
        else
            Y -= stepY;

        x.append(X);
        y.append(checkReload(Y + offset));

        X += stepX;
    }

    signalPlot.plotPtr->clearData();
    signalPlot.axisX->setRange(0, onePeriod*(double)currentTimeScaleFactor*PERIOD_NUM);
    signalPlot.axisX->setTickStep(onePeriod*(double)currentTimeScaleFactor*PERIOD_NUM/10);
    signalPlot.plotPtr->setData(x, y);
}

void SignalPlot::buildSquare(SIGNAL_CONFIG *s)
{
    x.clear();
    y.clear();

    double onePeriod = 1.0f/(s->currentFrequency);
    redefineTimeScale(onePeriod);
    double gain = s->gain;
    double stepX = onePeriod*(double)currentTimeScaleFactor/POINT_NUM;
    double offset = ((double)s->offset) * MAX_AMP_V/MAX_OFFSET_VALUE;

    double Y = MAX_AMP*gain;
    double X = 0;

    int periodCounter = 0;
    bool risingEdgeFlag = true;

    for(int i(0); i <= (POINT_NUM+1)*PERIOD_NUM; i++)
    {

        if(periodCounter >= POINT_NUM/2)
        {
            periodCounter = 0;
            risingEdgeFlag = !risingEdgeFlag;
        }

        if(risingEdgeFlag)
        {
            Y = MAX_AMP*gain;
        }
        else
        {
            Y = -MAX_AMP*gain;
        }

        x.append(X);
        y.append(checkReload(Y + offset));

        X += stepX;

        periodCounter++;
    }

    signalPlot.plotPtr->clearData();
    signalPlot.axisX->setRange(0, onePeriod*(double)currentTimeScaleFactor*PERIOD_NUM);
    signalPlot.axisX->setTickStep(onePeriod*(double)currentTimeScaleFactor*PERIOD_NUM/10);
    signalPlot.plotPtr->setData(x, y);
}

void SignalPlot::updatePlot(SIGNAL_CONFIG *s)
{
    switch(s->type)
    {
    case SINUS_SIGNAL:
        buildSinus(s);
        break;
    case SAW_SIGNAL:
        buildSAW(s);
        break;
    case REV_SAW_SIGNAL:
        buildRevSAW(s);
        break;
    case TRIANGLE_SIGNAL:
        buildTriangle(s);
        break;
    case SQUARE_SIGNAL:
        buildSquare(s);
        break;
    }

    plot->yAxis->rescale();
    double offset = ((double)s->offset) * MAX_AMP_V/MAX_OFFSET_VALUE;
    plot->yAxis->setRange(Y_MIN + offset, Y_MAX + offset);
    setFrequencyLabelText(s->currentFrequency);
    plot->replot();
}

void SignalPlot::setFrequencyLabelText(double freqHz)
{
    double freq_factor = 1;
    QString freqPostfix = tr("Hz");

    if(freqHz >= 1000)
    {
        if(freqHz >= 1000000)
        {
            freq_factor = 1000000;
            freqPostfix = tr("MHz");
        }
        else
        {
            freq_factor = 1000;
            freqPostfix = tr("KHz");
        }
    }

    frequencyLabel->setText(tr("Frequency: ") + QString::number(freqHz/freq_factor) + " " + freqPostfix);
    frequencyLabel->position->setCoords(plot->xAxis->range().center(), plot->yAxis->range().upper);
}

void SignalPlot::replot(QVector<double> *x, QVector<double> *y)
{
    signalPlot.plotPtr->clearData();
    signalPlot.plotPtr->setData(*x, *y);
    plot->replot();
}
