#ifndef LESO5_TYPES_H
#define LESO5_TYPES_H

enum SIGNAL_TYPE
{
    SINUS_SIGNAL = 0,
    SAW_SIGNAL = 1,
    REV_SAW_SIGNAL = 2,
    TRIANGLE_SIGNAL = 3,
    SQUARE_SIGNAL = 4
};

enum SIGNAL_CHANNEL_NUM {
    SIGNAL_A = 0,
    SIGNAL_B
};

struct SIGNAL_CONFIG
{
private:
    int color;
    const char *icon;
    SIGNAL_CHANNEL_NUM num;

public:
    SIGNAL_CONFIG() {
        currentFrequency = 20;
        gain = 1;
        offset = 0;
        type = SINUS_SIGNAL;
        num = SIGNAL_A;
    }

    int currentFrequency;
    double gain;
    int offset;
    SIGNAL_TYPE type;

    void setColor(int c) { color = c; }
    void setIcon(const char *iconName) { icon = iconName; }
    void setNum(SIGNAL_CHANNEL_NUM n) { num = n; }

    int getColor() const { return color; }
    const char *getIconName() const { return icon; }
    SIGNAL_CHANNEL_NUM getNum() const { return num; }
};

#define UI_TEXT_COLOR 0x912E6F

#define CHANNEL_A_COLOR 0xFF820A
#define CHANNEL_B_COLOR 0x11CA3B

#define MAX_FREQUENCY 20000000

#define MAX_AMP_V 6.0f

#define MIN_OFFSET_VALUE -32767
#define MAX_OFFSET_VALUE 32767

#endif // LESO5_TYPES_H
