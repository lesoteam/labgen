#ifndef SIGNALPLOT_H
#define SIGNALPLOT_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVector>
#include <cmath>

#include "leso5_types.h"
#include "lib/qcustomplot.h"

#define GRAPH_LINE_WIDTH 2.0f

#define Y_MAX MAX_AMP_V
#define Y_MIN -MAX_AMP_V
#define X_MAX 1.0f/MAX_FREQUENCY
#define X_MIN 0

#define PI 3.14f

#define PERIOD_NUM 4
#define POINT_NUM 500

#define RED_LINE_WIDTH 1.5f

#define MAX_AMP MAX_AMP_V

#define CHECK_RELOAD_DAC(VAL) \
    if(VAL >= Y_MAX) {\
        VAL = Y_MAX; }\
    if(VAL <= Y_MIN) {\
        VAL = Y_MIN; }

using std::sin;

struct SIGNAL_PLOT
{
    QCPGraph *plotPtr;
    QCPAxis *axisX;
    QCPAxis *axisY;
    QPen graphPen;
};

class SignalPlot : public QWidget
{

Q_OBJECT

    enum TIME_SCALE_FACTOR
    {
        TIME_SCALE_FACTOR_S = 1,
        TIME_SCALE_FACTOR_mS = 1000,
        TIME_SCALE_FACTOR_uS = 1000000
    };

    TIME_SCALE_FACTOR currentTimeScaleFactor;

    SIGNAL_CONFIG *signal;
    SIGNAL_PLOT signalPlot;

    QCPItemLine *upperRedLine, *lowerRedLine;

    QVector<double> x, y;

    QCustomPlot *plot;
    QHBoxLayout hLayout;

    void buildSinus(SIGNAL_CONFIG *s);
    void buildSAW(SIGNAL_CONFIG *s);
    void buildRevSAW(SIGNAL_CONFIG *s);
    void buildTriangle(SIGNAL_CONFIG *s);
    void buildSquare(SIGNAL_CONFIG *s);

    void redefineTimeScale(double period);
    void setFrequencyLabelText(double freqHz);

    QCPItemText *frequencyLabel;

    double checkReload(double val);

public:
    SignalPlot(SIGNAL_CONFIG *_signal);
    ~SignalPlot();

    QVector<double>* getXPoints() { return &x; }
    QVector<double>* getYPoints() { return &y; }

    void setVoltageRange(double amp_v);

public slots:
    void updatePlot(SIGNAL_CONFIG *s);
    void replot(QVector<double> *x, QVector<double> *y);
};

#endif // SIGNALPLOT_H
