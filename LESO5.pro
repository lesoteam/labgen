#-------------------------------------------------
#
# Project created by QtCreator 2016-02-16T12:35:01
#
#-------------------------------------------------

QT       += core gui printsupport serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LESO5
TEMPLATE = app


SOURCES += main.cpp\
    lib/qcustomplot.cpp \
    uartdevice.cpp \
    leso5abstractdevice.cpp \
    leso5mainwindow.cpp \
    devicesettingstoolbar.cpp \
    signalplot.cpp \
    leso5channelwidget.cpp \
    menu/normalmodepopupmenu.cpp \
    menu/gainpopupmenu.cpp

HEADERS  += \
    lib/qcustomplot.h \
    uartdevice.h \
    leso5_types.h \
    leso5abstractdevice.h \
    leso5mainwindow.h \
    devicesettingstoolbar.h \
    signalplot.h \
    leso5channelwidget.h \
    menu/normalmodepopupmenu.h \
    menu/gainpopupmenu.h

FORMS    += \
    leso5channelcontrol.ui \
    leso5mainwindow.ui

INCLUDEPATH += ./

RESOURCES += \
    leso5resource.qrc
