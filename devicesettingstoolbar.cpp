#include "devicesettingstoolbar.h"

DeviceSettingsToolBar::DeviceSettingsToolBar(LESO5AbstractDevice *devPtr, QLabel *statusLabelPtr, QLabel *deviceIDLabePtr)
{
    dev = devPtr;
    status = statusLabelPtr;
    deviceID = deviceIDLabePtr;

    portList = new QComboBox;
    addWidget(portList);

    for(int i(0); i <= 10; i++)
        portList->addItem("COM" + QString::number(i+1));

    openCloseButton = new QPushButton(tr("Open"));
    addWidget(openCloseButton);

    connect(openCloseButton, SIGNAL(clicked()), this, SLOT(openPort()));

    setMovable(false);
}

DeviceSettingsToolBar::~DeviceSettingsToolBar()
{

}

void DeviceSettingsToolBar::openPort()
{
    if(dev->isOpen())
    {
        dev->closeDevice();
        openCloseButton->setText(tr("Open"));
        deviceID->setText("");
        status->setText("Device Closed");
    }
    else
    {
        dev->openDevice(portList->currentText());

        if(!dev->isOpen()) return;

        if(!dev->DeviceID().isEmpty())
            deviceID->setText(dev->DeviceID());

        status->setText(tr("Device Opened"));
        deviceID->setText(dev->DeviceID());
        openCloseButton->setText(tr("Close"));
    }
}

void DeviceSettingsToolBar::setToolBarColor(int color)
{
    QColor c(color);
    setStyleSheet("QComboBox, QWidget, QToolTip, QPushButton { color: #" +
                  QString::number(c.rgb(), 16) + ";"
                  "font: bold 16px; spacing: 10px;"
                  "selection-color: blue;"
                  "selection-background-color: yellow; }"
                  "QPushButton { width: 50px; } ");
}
