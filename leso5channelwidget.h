#ifndef LESO5CHANNELWIDGET_H
#define LESO5CHANNELWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QToolButton>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>

#include "uartdevice.h"
#include "signalplot.h"
#include "menu/normalmodepopupmenu.h"
#include "menu/gainpopupmenu.h"
#include "leso5_types.h"

class LESO5ChannelWidget : public QWidget
{
    Q_OBJECT

    QHBoxLayout *hLayout;
    QVBoxLayout *vLayout;

    LESO5AbstractDevice *dev;
    SIGNAL_CONFIG *signal;
    SignalPlot *plot;

    QComboBox *signalType;
    QDoubleSpinBox *gainBox;
    QSpinBox *freqBox;
    QComboBox *freqMultBox;
    QToolButton *settingsButton;
    QToolButton *gainButton;

    NormalModePopupMenu *settingsMenu;
    GainPopupMenu *gainMenu;

    enum max_amplitude_gain_t
    {
        max_amplitude_gain_1 = 0,
        max_amplitude_gain_05 = 1,
        max_amplitude_gain_025 = 2,
        max_amplitude_gain_01 = 3
    };

    max_amplitude_gain_t currentAmplitude;

public:
    explicit LESO5ChannelWidget(LESO5AbstractDevice *_dev, SIGNAL_CONFIG *_signal, QWidget *parent = 0);
    ~LESO5ChannelWidget();

    void setColorStyle(int color);
    void setMaxAmp(max_amplitude_gain_t amp);

public slots:
    void showSettingsMenu();
    void resetSettings();
    void changeFrequency(int freq);
    void changeFrequencyMult(int comboBoxIndex);
    void changeSignalType(int comboBoxIndex);
    void changeGain(double gain);

signals:
    void changeSignalConfig(SIGNAL_CONFIG *s);
};

#endif // LESO5CHANNELWIDGET_H
