#include "leso5channelwidget.h"

LESO5ChannelWidget::LESO5ChannelWidget(LESO5AbstractDevice *_dev, SIGNAL_CONFIG *_signal, QWidget *parent)
    : QWidget(parent)
{
    dev = _dev;
    signal = _signal;
    currentAmplitude = max_amplitude_gain_1;

    hLayout = new QHBoxLayout;
    vLayout = new QVBoxLayout;

    plot = new SignalPlot(signal);

    settingsMenu = new NormalModePopupMenu(dev, signal, plot);

    settingsButton = new QToolButton;
    settingsButton->setToolTip(tr("Setting waveform"));
    settingsButton->setBaseSize(50, 50);
    settingsButton->setIcon(QIcon(":/buttons/icon/" + QString(signal->getIconName())));
    settingsButton->setMenu(settingsMenu);
    hLayout->addWidget(settingsButton);

    gainMenu = new GainPopupMenu;
    gainButton = new QToolButton;
    gainButton->setText(tr("G"));
    gainButton->setToolTip(tr("Signal gain"));
    gainButton->setMenu(gainMenu);
    hLayout->addWidget(gainButton);

    signalType = new QComboBox;
    signalType->setToolTip(tr("Select signal type"));
    signalType->addItem(tr("Sinus"));
    signalType->addItem(tr("SAW"));
    signalType->addItem(tr("Rev SAW"));
    signalType->addItem(tr("Triangle"));
    signalType->addItem(tr("Square"));

    hLayout->addWidget(signalType);

    /*gainBox = new QDoubleSpinBox;
    gainBox->setToolTip(tr("Signal gain"));
    gainBox->setMinimum(0.001f);
    gainBox->setMaximum(1);
    gainBox->setSingleStep(0.01);
    gainBox->setValue(1);

    hLayout->addWidget(gainBox);*/

    freqBox = new QSpinBox;
    freqBox->setToolTip(tr("Frequency (Hz)"));
    freqBox->setMinimum(1);
    freqBox->setMaximum(20);
    freqBox->setValue(20);
    hLayout->addWidget(freqBox);

    freqMultBox = new QComboBox;
    freqMultBox->setToolTip(tr("Frequency multiplication select"));
    freqMultBox->addItem("x1");
    freqMultBox->addItem("x10");
    freqMultBox->addItem("x100");
    freqMultBox->addItem("x1000");
    freqMultBox->addItem("x10000");
    freqMultBox->addItem("x100000");
    hLayout->addWidget(freqMultBox);

    vLayout->addLayout(hLayout);
    vLayout->addWidget(plot);
    settingsMenu->setMainPlot(plot);

    setLayout(vLayout);

    setColorStyle(signal->getColor());

    connect(freqBox, SIGNAL(valueChanged(int)), this, SLOT(changeFrequency(int)));
    connect(freqMultBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeFrequencyMult(int)));
    connect(signalType, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSignalType(int)));
    //connect(gainBox, SIGNAL(valueChanged(double)), this, SLOT(changeGain(double)));
    connect(settingsButton, SIGNAL(clicked(bool)), settingsButton, SLOT(showMenu()));
    //connect(settingsButton, SIGNAL(clicked(bool)), this, SLOT(showSettingsMenu()));
    connect(gainButton, SIGNAL(clicked(bool)), gainButton, SLOT(showMenu()));
    connect(gainMenu, SIGNAL(changeGain(double)), this, SLOT(changeGain(double)));
}

LESO5ChannelWidget::~LESO5ChannelWidget()
{

}

void LESO5ChannelWidget::showSettingsMenu()
{
    //QPoint p = this->pos();
    //QRect geo = settingsButton->geometry();
    //qDebug() << "Show: " << parentWidget()->geometry();
    //settingsMenu->exec(QPoint(parentWidget()->pos().x() + settingsButton->pos().x(),
    //                         parentWidget()->pos().y() + settingsButton->pos().y()));
}

void LESO5ChannelWidget::setColorStyle(int color)
{
    QColor c(color);
    setStyleSheet("QComboBox, QSpinBox, QDoubleSpinBox, QWidget, QToolTip, QToolButton { color: #" +
                  QString::number(c.rgb(), 16) + ";"
                  "font: bold 16px; spacing: 10px;"
                  "selection-color: blue; selection-background-color: yellow; }"
                  "QToolButton:pressed { border: 5px solid #" + QString::number(c.rgb(), 16) + ";}"
                  " QToolButton { padding: 5px; }");
}

void LESO5ChannelWidget::resetSettings()
{
    freqMultBox->setCurrentIndex(0);
    freqBox->setValue(20);
    gainBox->setValue(1);
    signalType->setCurrentIndex(0);
}

void LESO5ChannelWidget::changeFrequency(int freq)
{
    int freq_mult_factor = 1;

    switch(freqMultBox->currentIndex())
    {
    case 0: freq_mult_factor = 1; break;
    case 1: freq_mult_factor = 10; break;
    case 2: freq_mult_factor = 100; break;
    case 3: freq_mult_factor = 1000; break;
    case 4: freq_mult_factor = 10000; break;
    case 5: freq_mult_factor = 100000; break;
    case 6: freq_mult_factor = 1000000; break;
    }

    signal->currentFrequency = freq * freq_mult_factor;

    dev->setFrequency(signal->currentFrequency, signal->getNum());

    plot->updatePlot(signal);
    settingsMenu->updatePlot();
}

void LESO5ChannelWidget::changeFrequencyMult(int comboBoxIndex)
{
    int freq_mult_factor = 1;

    switch(comboBoxIndex)
    {
    case 0: freq_mult_factor = 1; break;
    case 1: freq_mult_factor = 10; break;
    case 2: freq_mult_factor = 100; break;
    case 3: freq_mult_factor = 1000; break;
    case 4: freq_mult_factor = 10000; break;
    case 5: freq_mult_factor = 100000; break;
    case 6: freq_mult_factor = 1000000; break;
    }

    signal->currentFrequency = freqBox->value() * freq_mult_factor;

    dev->setFrequency(signal->currentFrequency, signal->getNum());

    plot->updatePlot(signal);
    settingsMenu->updatePlot();
}

void LESO5ChannelWidget::changeSignalType(int comboBoxIndex)
{
    SIGNAL_TYPE type;

    switch(comboBoxIndex)
    {
    case 0: type = SINUS_SIGNAL; break;
    case 1: type = SAW_SIGNAL; break;
    case 2: type = REV_SAW_SIGNAL; break;
    case 3: type = TRIANGLE_SIGNAL; break;
    case 4: type = SQUARE_SIGNAL; break;
    }

    signal->type = type;
    dev->setSignalWaveform(type, signal->getNum());

    plot->updatePlot(signal);
    settingsMenu->updatePlot();
}

void LESO5ChannelWidget::changeGain(double gain)
{
    signal->gain = gain;
    dev->setGain(gain, signal->getNum());

    /*if(gain > 0.09)
    {
        if(gain > 0.24)
        {
            gainMenu->setMaxGain(0.5);
        }
        if(gain > 0.45)
            gainMenu->setMaxGain(1);

        gainMenu->setMaxGain(0.25f);
    }
    else
        gainMenu->setMaxGain(0.1f);*/

    plot->updatePlot(signal);
    settingsMenu->updatePlot();
}

void LESO5ChannelWidget::setMaxAmp(max_amplitude_gain_t amp)
{
    switch(amp)
    {
    case max_amplitude_gain_1: {
        //gainBox->setMinimum(0.1f);
        //gainBox->setSingleStep(0.01f);
        //gainBox->update();
        plot->setVoltageRange(MAX_AMP_V);
        gainMenu->setMaxGain(1);
    }
        break;
    case max_amplitude_gain_05: {
        //gainBox->setMinimum(0.001f);
        //gainBox->setSingleStep(0.001f);
        plot->setVoltageRange(MAX_AMP_V/2);
        //gainBox->update();
        gainMenu->setMaxGain(0.5);
    }
        break;
    case max_amplitude_gain_025: {
        plot->setVoltageRange(MAX_AMP_V/4);
        gainMenu->setMaxGain(0.25);
    }
        break;

    case max_amplitude_gain_01: {
        plot->setVoltageRange(MAX_AMP_V/6);
        gainMenu->setMaxGain(0.1);
    }
        break;
    }
}
