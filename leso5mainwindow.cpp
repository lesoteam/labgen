#include "leso5mainwindow.h"
#include "ui_leso5mainwindow.h"

LESO5MainWindow::LESO5MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LESO5MainWindow)
{
    ui->setupUi(this);

    channelASettings.setNum(SIGNAL_A);
    channelBSettings.setNum(SIGNAL_B);
    channelASettings.setColor(CHANNEL_A_COLOR);
    channelBSettings.setColor(CHANNEL_B_COLOR);
    channelASettings.setIcon("menuButton");
    channelBSettings.setIcon("menuButton");

    settingsBox = new DeviceSettingsToolBar(&device, &statusText, &deviceID);
    settingsBox->setToolBarColor(UI_TEXT_COLOR);
    addToolBar(settingsBox);

    ui->statusbar->addWidget(&statusText);
    statusText.setText(tr("Device Closed"));
    ui->statusbar->addWidget(&deviceID);

    channelA = new LESO5ChannelWidget(&device, &channelASettings);
    channelB = new LESO5ChannelWidget(&device, &channelBSettings);

    centralWidget = new QMdiArea;
    setCentralWidget(centralWidget);

    QMdiSubWindow *windowA = centralWidget->addSubWindow(channelA, Qt::WindowMinMaxButtonsHint);
    QMdiSubWindow *windowB = centralWidget->addSubWindow(channelB, Qt::WindowMinMaxButtonsHint);

    centralWidget->tileSubWindows();

    windowA->setWindowTitle(tr("Channel A"));
    windowB->setWindowTitle(tr("Channel B"));
    centralWidget->setDocumentMode(true);
}

LESO5MainWindow::~LESO5MainWindow()
{
    delete ui;
}
