#include "gainpopupmenu.h"

GainPopupMenu::GainPopupMenu()
{
    setLayout(&mainLayout);

    setMinimumHeight(200);
    setMaximumWidth(500);

    gainSpinBox = new QDoubleSpinBox;
    gainSpinBox->setMaximum(MAX_GAIN);
    gainSpinBox->setMinimum(MIN_GAIN);
    gainSpinBox->setSingleStep(MIN_GAIN);
    gainSpinBox->setDecimals(5);
    //gainSpinBox->setFixedWidth(50);

    mainLayout.addWidget(gainSpinBox);

    gainSlider = new QSlider;
    gainSlider->setOrientation(Qt::Vertical);
    gainSlider->setMaximum(MAX_GAIN/MIN_GAIN);
    gainSlider->setMinimum(1);
    gainSlider->setSingleStep(1);
    sliderLayout.addWidget(gainSlider);

    //mainLayout.addWidget(gainSlider);
    mainLayout.addLayout(&sliderLayout);

    connect(gainSlider, SIGNAL(valueChanged(int)), this, SLOT(changeGainSliderValue(int)));
    connect(gainSpinBox, SIGNAL(valueChanged(double)), this, SLOT(changeGainSpinBoxValue(double)));

    gainSpinBox->setValue(1);
}

void GainPopupMenu::changeGainSliderValue(int val)
{
    double gain = val / (MAX_GAIN/MIN_GAIN);
    gainSpinBox->setValue(gain);
    emit changeGain(gain);
}

void GainPopupMenu::changeGainSpinBoxValue(double val)
{
    //qDebug() << val*(MAX_GAIN/MIN_GAIN);
    gainSlider->setValue(val*(MAX_GAIN/MIN_GAIN));
    emit changeGain(val);
}

void GainPopupMenu::setMaxGain(double m)
{
    gainSlider->setMaximum(m);
}
