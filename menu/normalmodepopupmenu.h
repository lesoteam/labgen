#ifndef NORMALMODEPOPUPMENU_H
#define NORMALMODEPOPUPMENU_H

#include <QMenu>
#include <QSlider>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QVector>
#include <QSpinBox>

#include <lib/qcustomplot.h>

#include "leso5abstractdevice.h"
#include "leso5_types.h"
#include "signalplot.h"

class NormalModePopupMenu : public QMenu
{
    Q_OBJECT

    QHBoxLayout mainLayout;
    QVBoxLayout controlLayout;
    QSlider *offsetSlider;
    QSpinBox *offsetSpinBox;
    QCustomPlot *plot;
    QCPGraph *mainGraph;

    LESO5AbstractDevice *dev;

    SIGNAL_CONFIG *signal;
    SignalPlot *signalPlot;

    double prevSliderValue;

protected:
    void showEvent(QShowEvent *e);

public:
    NormalModePopupMenu(LESO5AbstractDevice *_dev, SIGNAL_CONFIG *_signal, SignalPlot *_plot, QWidget *parent = 0);
    void setMainPlot(SignalPlot *mPlot) { signalPlot = mPlot; }

public slots:
    void changeOffset(int _offset);
    void updatePlot();
};

#endif // NORMALMODEPOPUPMENU_H
