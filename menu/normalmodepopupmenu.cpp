#include "normalmodepopupmenu.h"

NormalModePopupMenu::NormalModePopupMenu(LESO5AbstractDevice *_dev, SIGNAL_CONFIG *_signal, SignalPlot *_plot, QWidget *parent) :
    QMenu(parent)
{
    dev = _dev;
    signal = _signal;
    signalPlot = _plot;

    prevSliderValue = 0;

    setLayout(&mainLayout);

    offsetSlider = new QSlider;
    offsetSlider->setOrientation(Qt::Vertical);
    offsetSlider->setMinimum(MIN_OFFSET_VALUE);
    offsetSlider->setMaximum(MAX_OFFSET_VALUE);
    offsetSlider->setTracking(true);

    offsetSpinBox = new QSpinBox;
    offsetSpinBox->setMaximum(MAX_OFFSET_VALUE);
    offsetSpinBox->setMinimum(MIN_OFFSET_VALUE);
    offsetSpinBox->setValue(0);
    controlLayout.addWidget(offsetSpinBox);
    controlLayout.addWidget(offsetSlider);

    mainLayout.addLayout(&controlLayout);

    plot = new QCustomPlot;
    plot->yAxis->setRange(-MAX_AMP_V, MAX_AMP);
    plot->yAxis->setLabel(tr("Amplitude (V)"));

    mainGraph = plot->addGraph();
    mainGraph->setPen(QPen(Qt::red));
    mainGraph->setData(*(signalPlot->getXPoints()), *(signalPlot->getYPoints()));
    plot->xAxis->setRange(0, 200);

    mainLayout.addWidget(plot);

    setMinimumSize(500, 200);
    mainLayout.setSpacing(20);

    setWindowFlags(Qt::Popup);

    connect(offsetSlider, SIGNAL(valueChanged(int)), this, SLOT(changeOffset(int)));
    connect(offsetSpinBox, SIGNAL(valueChanged(int)), this, SLOT(changeOffset(int)));
    connect(offsetSlider, SIGNAL(valueChanged(int)), offsetSpinBox, SLOT(setValue(int)));
    connect(offsetSpinBox, SIGNAL(valueChanged(int)), offsetSlider, SLOT(setValue(int)));

    updatePlot();
}

void NormalModePopupMenu::showEvent(QShowEvent *e)
{
    Q_UNUSED(e);

    updatePlot();
}

void NormalModePopupMenu::changeOffset(int _offset)
{
    signal->offset = _offset;
    dev->setOffset(_offset, signal->getNum());
    updatePlot();
}

void NormalModePopupMenu::updatePlot()
{
    if(signalPlot == NULL)
    {
        qDebug() << "NormalModePopupWidget - not init main plot pointer !!!! ";
        return;
    }

    QVector<double> *X_vect = signalPlot->getXPoints();
    QVector<double> *Y_vect = signalPlot->getYPoints();

    double offset = ((double)signal->offset) * MAX_AMP_V/MAX_OFFSET_VALUE;
    //if(offset == prevSliderValue)
    //    return;

    for(int i(0); i < X_vect->size(); i++)
    {
        double y = Y_vect->at(i);
        Y_vect->replace(i, (y - prevSliderValue) + offset);
    }

    plot->xAxis->rescale();
    mainGraph->clearData();
    mainGraph->setData(*X_vect, *Y_vect);

    plot->replot();
    signalPlot->updatePlot(signal);
    //signalPlot->replot(X_vect, Y_vect);
    prevSliderValue = offset;
}
