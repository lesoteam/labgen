#ifndef GAINPOPUPMENU_H
#define GAINPOPUPMENU_H

#include <QMenu>
#include <QSlider>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDoubleSpinBox>
#include <QDebug>

#define MAX_GAIN 1
#define MIN_GAIN 0.0001f

class GainPopupMenu : public QMenu
{
    Q_OBJECT

    QVBoxLayout mainLayout;
    QHBoxLayout sliderLayout;

    QDoubleSpinBox *gainSpinBox;
    QSlider *gainSlider;

public:
    GainPopupMenu();

public slots:
    void changeGainSliderValue(int val);
    void changeGainSpinBoxValue(double val);
    void setMaxGain(double m);

signals:
    void changeGain(double gain);
};

#endif // GAINPOPUPMENU_H
