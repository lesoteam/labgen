#ifndef DEVICESETTINGSTOOLBAR_H
#define DEVICESETTINGSTOOLBAR_H

#include <QToolBar>
#include <QMainWindow>
#include <QComboBox>
#include <QPushButton>
#include <QLabel>

#include "leso5_types.h"
#include "leso5abstractdevice.h"

class DeviceSettingsToolBar : public QToolBar
{
    Q_OBJECT

    LESO5AbstractDevice *dev;
    QMainWindow *mainWindowPtr;

    QPushButton *openCloseButton;
    QComboBox *portList;

    QLabel *status;
    QLabel *deviceID;

public:
    DeviceSettingsToolBar(LESO5AbstractDevice *devPtr, QLabel *statusLabelPtr, QLabel *deviceIDLabePtr);
    ~DeviceSettingsToolBar();
    void setToolBarColor(int color);

public slots:
    void openPort();
};

#endif // DEVICESETTINGSTOOLBAR_H
