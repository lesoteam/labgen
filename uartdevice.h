#ifndef UARTDEVICE_H
#define UARTDEVICE_H

#include <QDebug>
#include <QSerialPort>
#include <cmath>

#include "leso5_types.h"
#include "leso5abstractdevice.h"

#define UART_BR 115200

#define ADDR_LED_CONTROL		0xFF
#define ADDR_VERSION_REQUEST	0xFE

#define ADDR_NCOA_FREQ1_R0		0x00
#define ADDR_NCOA_FREQ1_R1		0x01
#define ADDR_CHANA_GAIN			0x30
#define ADDR_CHANA_OFFSET		0x31
#define ADDR_MUX_A 0x32

#define ADDR_NCOB_FREQ1_R0		0x80
#define ADDR_NCOB_FREQ1_R1		0x81
#define ADDR_CHANB_GAIN			0xB0
#define ADDR_CHANB_OFFSET		0xB1
#define ADDR_MUX_B 0xB2

#define ADDR_NCO_FORM			0x50

#define ADDR_NCO_ACC_RESET		0x0C

#define ADDR_CHANA_MUX			0x0D
#define ADDR_CHANB_MUX			0x0E

#define CMD_BYTE_NUM 3

#define DAC_FREQ 125000000

#define SEND_CMD(cmd) serialPort.write(cmd, CMD_BYTE_NUM)

using std::pow;


/*Значение для записи вычисляется следующим образом:
(f * 2^32) / Fdac = REG

где f -- требуемая частота в Гц, Fdac -- частота дискретизации в Гц, REG -- значение регистра.*/

/*D = (Fdac/f*2) -1;
 Fdac -- частота DAC;
 f --  требуемая частота

*/

class UARTDevice : public LESO5AbstractDevice
{
    Q_OBJECT

    QString deviceID;
    void getDeviceID();
    QSerialPort serialPort;

public:
    UARTDevice();
    ~UARTDevice();

    bool openDevice(QString portName);
    void closeDevice();

    void setFrequency(int freq, SIGNAL_CHANNEL_NUM channel, bool setDividerReg = true);
    void setOffset(int offset, SIGNAL_CHANNEL_NUM channel);
    void setGain(double amp, SIGNAL_CHANNEL_NUM channel);
    void setSignalWaveform(SIGNAL_TYPE type, SIGNAL_CHANNEL_NUM channel);
    void setPhaseOffset(int phase, SIGNAL_CHANNEL_NUM channel);
    bool isOpen() { return LESO5AbstractDevice::isOpen(); }
    QString DeviceID() { return deviceID; }

};

#endif // UARTDEVICE_H
